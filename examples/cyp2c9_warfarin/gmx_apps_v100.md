# APPS集群GROMACS GPU加速测试

APPS集群cu04上有两块Tesla V100 GPU卡，CPU核为48。

```
[xushun@cu04 cyp2c9_warfarin]$ nvidia-smi -L
GPU 0: Tesla V100-PCIE-32GB (UUID: GPU-d586b14e-d981-810a-236b-f69eb681369c)
GPU 1: Tesla V100-PCIE-32GB (UUID: GPU-124347b5-9d57-337f-4ffa-d95db2c31541)
```

集群上安装了GROMACS 单节点的Thread MPI 和多节点OpenMPI 的版本，路径为

```
GROMACS:      gmx mdrun, version 2020
Executable:   /opt/soft/gromacs-2020.3-tmpi-cu04/bin/gmx
Executable:  /opt/soft/gromacs-2020.3-mpi-cu04/bin/gmx_mpi
```

现针对cyp2c9 warfarin体系进行测试，包含原子数 57732。

## 一、cu04单节点，Thread MPI测试

加载环境变量

```
module purge
module add compiler/gcc/8.3.0
module add gromacs/2020.3_tmpi_cu04
```

### 1. 默认任务分配给GPU方式

```
gmx mdrun -dlb yes -pin on -ntmpi 1  -v -deffnm cyp -s run.tpr -ntomp 24 -nsteps 10000
```



日志输出：

```
1 GPU selected for this run.
Mapping of GPU IDs to the 2 GPU tasks in the 1 rank on this node:
  PP:0,PME:0
PP tasks will do (non-perturbed) short-ranged interactions on the GPU
PME tasks will do all aspects on the GPU
Using 1 MPI process
Using 24 OpenMP threads
```

性能结果：

```
step 10000, remaining wall clock time:     0 s
               Core t (s)   Wall t (s)        (%)
       Time:      307.134       12.803     2398.9
                 (ns/day)    (hour/ns)
Performance:      134.978        0.178
```

可以看出该体系的默认方式中 short-ranged和PME都在GPU上计算。在1~48之间调节-ntomp选项的值，发现-ntomp 24性能最优，即一半核数时性能最优。

### 2. 大部分任务分配给GPU方式

```
gmx mdrun -dlb yes -pin on -ntmpi 1 -v -nb gpu -bonded gpu -pme gpu -update gpu -deffnm cyp -s run.tpr -ntomp 24 -nsteps 10000
```

日志输出：

```
1 GPU selected for this run.
Mapping of GPU IDs to the 2 GPU tasks in the 1 rank on this node:
  PP:0,PME:0
PP tasks will do (non-perturbed) short-ranged and most bonded interactions on the GPU
PME tasks will do all aspects on the GPU
Using 1 MPI process
Using 24 OpenMP threads
```

性能结果：

```
step 10000, remaining wall clock time:     0 s
               Core t (s)   Wall t (s)        (%)
       Time:      246.871       10.293     2398.5
                 (ns/day)    (hour/ns)
Performance:      167.902        0.143
```

可以看出该体系的除了 short-ranged和PME都在GPU上计算，most bonded计算也在GPU上计算，性能有所提升34 ns/day。

另外，使用-ntmpi 2选项调用两块GPU同时计算，性能不如单块GPU计算。

## 二、cu04单节点，（标准OpenMPI）MPI测试

加载环境变量

```
module purge
module add compiler/gcc/8.3.0
module add gromacs/2020.3_tmpi_cu04
module add compiler/openmpi/4.1.5_GNU8.3
```

分布运行对应的如下两条命令

```
gmx_mpi mdrun -dlb yes -pin on -v -deffnm cyp -s run.tpr -ntomp 24 -nsteps 10000
gmx_mpi mdrun -dlb yes -pin on -v -nb gpu -bonded gpu -pme gpu -update gpu -deffnm cyp -s run.tpr -ntomp 24 -nsteps 10000
```

执行命令改为gmx_mpi，不再需要添加-ntmpi 1选项。
测试的性能结果和 Thread MPI测试的性能结果一个水平。